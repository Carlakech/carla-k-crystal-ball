package com.example.student.myapplication;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
              "Your wishes will come true.",
                "your wishes will never come true.",
                "you are amazing",
                "Try again soon",
                "You rock!"
    };

}
    public static Predictions get () {
        if(predictions == null){
            predictions = new Predictions();
        }
        return predictions;

    }

    public String getPrediction() {


              //Math.rand stuff
        int range = (answers.length - 1);
                int rand = (int)(Math.random() * range) + 1;
               return answers[rand];
    }
}
